How to run
----------

To reproduce the experiments, please use the following entry-point script and command:

sh scripts/rl-runAll-experiments.sh

The output of the various experiments are placed in the folders listed below:

resultsExperimentsW100/
resultsExperimentsScalability/

Output files
------------

1.results_experiments.csv  			# Run (test) simulation results 
1.results_experiments_selected.csv		# Best-performing delta per workflow and strategy - Test

1.training_results_experiments.csv  		# Training simulation results
1.training_results_experiments_selected.csv 	# Best-performing delta per workflow and strategy - Training

2.analysis_mean-performace-strategies.csv  	# Tableau-ready raw data
2.analysis_aggregatedMC-median-comp-test.csv 	# Statistical significance results

executedExperiments 				# Executed configurations
trainingInfo.csv 				# Elapsed runtime information

Project structure
-----------------

data/instances/ 		# VM info
lib/ 				# Library dependencies (JAR format)
logs/ 				# Execution logs
qTables/ 			# qTables
qTablesSum/ 			# qTablesSummary
resultsExperimentsW100/ 	# Experiment results (full-size workflows; W100 as identified in the paper) 
resultsExperimentsScalability/ # Scalability experiment results (small to medium-sized workflows; W25-50-75 as identified in the paper)
scripts/ 			# Python and command-line scripts
workflows/ 			# Workflow information

AutoscalingWfRL-0.0.1.jar 	# Project compiled code (JAR format)
cases*				# Run configurations (per workflow) for RL learning (training and testing) and for competitors

Execution details and used libraries
------------------------------------

The experiments reported in the paper were run on a Intel Core i7 computer running Ubuntu Desktop 18.04.5 and Java version 9. 
The description of the four workflows studied are available on-line through the Pegasus WorkflowGenerator: https://confluence.pegasus.isi.edu/display/pegasus/WorkflowHub. 
The implementation of the Q-learning algorithm was provided by the Brown-UMBC Reinforcement Learning and Planning (BURLAP) (http://burlap.cs.brown.edu/) Java code library version 3.0. 
Simulations were performed using the CloudSim (http://www.cloudbus.org/cloudsim/) simulator version 3.0. 
The statistical significance of the results was assessed using the implementation of the Mann–Whitney U test incorporated in Apache Commons Math (http://commons.apache.org/proper/commons-math/) version 3.3. 
Finally, Tableau Public (http://public.tableau.com) version 2018.3 and matplotlib (https://matplotlib.org/) version 3.2.2 were used to generate the visualizations.