#!/bin/bash

logsPath= ./logs
r1Path= ./resultsExperimentsW100
r2Path= ./resultsExperimentsScalability

qPath25= ./qTables/w25/
qPath50= ./qTables/w50/
qPath75= ./qTables/w75/
qPath100= ./qTables/w100/

qPathSum25= ./qTablesSum/w25/
qPathSum50= ./qTablesSum/w50/
qPathSum75= ./qTablesSum/w75/
qPathSum100= ./qTablesSum/w100/

for dir in $logsPath $r1Path $r2Path $qPath25 $qPath50 $qPath75 $qPath100 $qPathSum25 $qPathSum50 $qPathSum75 $qPathSum100
do
    rm $dir/*	
done
