import pandas as pd
import numpy as np

# this assumes that your file is comma separated
# if it is e.g. tab separated you should use pd.read_csv('data.csv', sep = '\t')
df = pd.read_csv('./1.training_results_experiments_selected.csv')


strategy_alias = {"rlaSchECU": "Prop-ECU", "rlaBaselineSchECU": "Base-ECU", "rlaSchPrice": "Prop-Price", "rlaBaselineSchPrice": "Base-Price","rlaSchRandom": "Prop-Rand","rlaBaselineSchRandom": "Base-Rand"}

# select desired columns
df = df[['case', 'workflow','strategy','autoscalerArgs','makespan','totalCost','totalReward','episode','policy','epsilon']]
df['alpha'] = [args.split(':')[6] for args in df['autoscalerArgs']]
#df['aggMC'] = [np.linalg.norm((df['makespan'], df['totalCost']))]
df['aggMC'] = np.sqrt(df['makespan']/3600**2 + df['totalCost']**2)
df['strategy'] = [strategy_alias[s] for s in df['strategy']]
df['strategyAlpha'] = [row[1]['strategy'] +'('+row[1]['alpha']+')' if 'Prop' in row[1]['strategy'] else row[1]['strategy'] for row in df.iterrows()]

#write to the file (tab separated)
df.to_csv('./1.training_results_experiments_selected_f.csv', sep=',', index=False)
