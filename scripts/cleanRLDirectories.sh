#!/bin/bash

rm logs/*

rm qTables/w30/*
rm qTables/w40/*
rm qTables/w50/*
rm qTables/w60/*
rm qTables/w70/*
rm qTables/w80/*
rm qTables/w90/*
rm qTables/w100/*

rm qTablesSum/w30/*
rm qTablesSum/w40/*
rm qTablesSum/w50/*
rm qTablesSum/w60/*
rm qTablesSum/w70/*
rm qTablesSum/w80/*
rm qTablesSum/w90/*
rm qTablesSum/w100/*

rm resultsExperimentsScalability/*
rm resultsExperimentsW100/*

