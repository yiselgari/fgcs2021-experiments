#!/bin/bash

#cleaning directories [logs, qTables, qTablesSum, resultsExperimentsScalability, resultsExperimentsW100]
sh scripts/cleanRLDirectories.sh

#running training and testing experiments W100
python scripts/simulate.py 4 casesTrainW100
python scripts/simulate.py 4 casesTestW100
python scripts/simulate.py 4 casesTestSiaaNoSpots

#running analyses W100
java -Xmx1024m -cp "./AutoscalingWfRL-0.0.1.jar" study023.analysis.LaunchAnalyses_W100

#moving results
r1Path="./resultsExperimentsW100/"
mv 1.training_results_experiments.csv $r1Path
mv 1.training_results_experiments_selected.csv $r1Path
mv 1.results_experiments.csv $r1Path
mv 1.results_experiments_selected.csv $r1Path
mv 2.analysis_aggregatedMC-median-comp-test.csv $r1Path
mv 2.analysis_mean-performace-strategies.csv $r1Path
mv executedExperiments $r1Path
mv trainingInfo.csv $r1Path

#running training and testing experiments Scalability
python scripts/simulate.py 4 casesTrainW30
python scripts/simulate.py 4 casesTrainW40
python scripts/simulate.py 4 casesTrainW50
python scripts/simulate.py 4 casesTrainW60
python scripts/simulate.py 4 casesTrainW70
python scripts/simulate.py 4 casesTrainW80
python scripts/simulate.py 4 casesTrainW90

python scripts/simulate.py 4 casesTestW30
python scripts/simulate.py 4 casesTestW40
python scripts/simulate.py 4 casesTestW50
python scripts/simulate.py 4 casesTestW60
python scripts/simulate.py 4 casesTestW70
python scripts/simulate.py 4 casesTestW80
python scripts/simulate.py 4 casesTestW90
python scripts/simulate.py 4 casesTestW100b

mv 1.results_experiments.csv 1.results_experiments_selected.csv
mv 1.training_results_experiments.csv 1.training_results_experiments_selected.csv

#running analyses Scalability
java -Xmx1024m -cp "AutoscalingWfRL-0.0.1.jar" study023.analysis.LaunchAnalyses_Scalability

#moving results
r2Path="./resultsExperimentsScalability/"
mv 1.training_results_experiments_selected.csv $r2Path
mv 1.results_experiments_selected.csv $r2Path
mv 2.analysis_aggregatedMC-median-comp-test.csv $r2Path
mv executedExperiments $r2Path
mv trainingInfo.csv $r2Path

echo "Experiments completed, check outputDirectories:" $r1Path "," $r2Path
