import pandas as pd
import seaborn as sns
import matplotlib
from matplotlib import pyplot as plt


#matplotlib.use('TkAgg')

# data
# 1.results_experiments.csv (corridas de evaluación de las políticas de Prop y Base en las 3 variantes 25-50, 50-75, 75-100)
# 1.results_experiments_f.csv (results versión procesada con: aggMC, strategy(alias),#trainTasks,#testTasks)
# 2.analysis_aggregatedMC-median-comp-test.csv (significancia Prop vs Base)

#r_df = pd.read_csv('data/1.results_experiments.csv')
r2_df = pd.read_csv('1.results_experiments_selected_f.csv')
#a_df = pd.read_csv('data/2.analysis_aggregatedMC-median-comp-test.csv')


def _get_wf_training_size(s):
    return int(s.split(':')[1][1:])
    
def _get_wf_type(s):
    return s.split('-')[0]


r2_df['trainSize'] = r2_df['autoscalerArgs'].apply(_get_wf_training_size)
r2_df['wfType'] = r2_df['workflow'].apply(_get_wf_type)
data = r2_df

sns.relplot(x='trainSize', y='aggMC', data=data,
             err_style='bars',
             hue='strategy',
             col= 'wfType',
             kind="line",
             facet_kws={'sharey': False, 'sharex': True, 'legend_out':False})
plt.show()
