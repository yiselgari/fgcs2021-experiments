#!/bin/bash

path_output=""
path_input=""

jarpath=$path_input"./AutoscalingWfRL-0.0.1.jar"

# receive simulation case parameters
caseParameters=$1

caseID=`echo $caseParameters | sed "s/\-//g" | sed "s/ //g"`
matches=`cat executedExperiments | grep "$caseID" | wc -l`
if [ "$matches" != "0" ]
then
	echo "> already executed case, skipping... ($caseParameters)"
else
	echo ""
	echo "*****"
	echo "STARTING SIMULATION: $caseParameters"
	echo "*****"
	java -Xmx1024m -cp "$jarpath" study023.experiments.StartSingleSimulation2 $caseParameters
	echo $caseID >> executedExperiments
fi
rm sim_trace 2> /dev/null
rm sim_report 2> /dev/null

echo "DONE"
