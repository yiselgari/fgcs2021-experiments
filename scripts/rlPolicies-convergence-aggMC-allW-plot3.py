import matplotlib.pyplot as plt
import pandas as pd
import sys
import seaborn as sns

df = pd.read_csv("./1.training_results_experiments_selected_f.csv")

strategy1 = sys.argv[1]
strategy2 = sys.argv[2] #baseline

#filtering dataFrame
df = df[df['strategy'].isin([strategy1,strategy2])]

palette ={"Prop-ECU(0.3)": "C0", "Prop-ECU(0.5)": "C0", "Prop-ECU(0.7)": "C0", "Prop-Rand(0.3)": "C0", "Prop-Rand(0.5)": "C0", "Prop-Rand(0.7)": "C0", "Prop-Price(0.3)": "C0", "Prop-Price(0.5)": "C0","Prop-Price(0.7)": "C0", "Base-ECU": "C1", "Base-Rand": "C1", "Base-Price": "C1"}


plot = sns.relplot(row="workflow", x="episode", y="aggMC", kind="line", data=df, hue="strategyAlpha", palette=palette, facet_kws={'sharey': False, 'sharex': True, 'legend_out':False});

plt.show()
