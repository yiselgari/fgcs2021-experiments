# A Q-learning Approach for the Autoscaling of Scientific Workflows in the Cloud - Experiments

This project contains the runnable code for the experiments of the work _"A Q-learning Approach for the Autoscaling of Scientific Workflows in the Cloud"_. 

## Description

**Abstract** Autoscaling strategies aim to exploit the elasticity, resource heterogeneity and varied prices options of a Cloud infrastructure to improve efficiency in the execution of resource-hungry applications such as scientific workflows.
Scientific workflows represent a special type of Cloud application with task dependencies, high-performance computational requirements and fluctuating workloads.
Hence, the amount and type of resources needed during workflow execution changes dynamically over time.
The well-known autoscaling problem comprises *(i)* scaling decisions, for adjusting the computing capacity of a virtualized infrastructure to meet the current demand of the application and *(ii)* task scheduling decisions, for assigning tasks to specific acquired resources for execution.
Both are highly complex sub-problems, even more because of the uncertainty inherent to the Cloud.
Reinforcement Learning (RL) provides a solid framework for decision-making problems in stochastic environments.
Therefore, RL offers a promising perspective for designing Cloud autoscaling strategies based on an online learning process.
In this work, we propose a novel formulation for the problem of infrastructure scaling in the Cloud as a Markov Decision Process, and we use Q-learning algorithm for learning scaling policies, while demonstrating that considering the specific characteristics of workflow applications when taking autoscaling decisions can lead to more efficient workflow executions.
Thus, our RL-based scaling strategy exploits the information available about workflow dependency structures.
Simulation experiments on four well-known workflows demonstrate significant gains (25%-55%) of our proposal in comparison with a similar state-of-the-art proposal.


**Contact information**:
 - Yisel Garí: ygari@uncu.edu.ar
 - David A. Monge: dmonge@uncu.edu.ar
 - Cristian Mateos: cristian.mateos@isistan.unicen.edu.ar


## Instructions

### How to run the code

To reproduce the experiments, please use the following entry-point script and command:

```shell
sh scripts/rl-runAll-experiments.sh
```

The output of the various experiments will be placed in the folders listed below:

```text
resultsExperimentsW100/
resultsExperimentsScalability/
```


### Output files

```text
1.results_experiments.csv  			# Run (test) simulation results
1.results_experiments_selected.csv		# Best-performing delta per workflow and strategy - Test

1.training_results_experiments.csv  		# Training simulation results
1.training_results_experiments_selected.csv 	# Best-performing delta per workflow and strategy - Training

2.analysis_mean-performace-strategies.csv  	# Tableau-ready raw data
2.analysis_aggregatedMC-median-comp-test.csv 	# Statistical significance results

executedExperiments 				# Executed configurations
trainingInfo.csv 				# Elapsed runtime information
```

### Project structure

```text
data/instances/ 		# VM info
lib/ 				# Library dependencies (JAR format)
logs/ 				# Execution logs
qTables/ 			# qTables
qTablesSum/ 			# qTablesSummary
resultsExperimentsW100/ 	# Experiment results (full-size workflows; W100 as identified in the paper)
resultsExperimentsScalability/  # Scalability experiment results (small to medium-sized workflows; W25-50-75 as identified in the paper)
scripts/ 			# Python and command-line scripts
workflows/ 			# Workflow information

AutoscalingWfRL-0.0.1.jar 	# Project compiled code (JAR format)
cases*				# Run configurations (per workflow) for RL learning (training and testing) and for competitors
```

### Execution details and used libraries

```text
The experiments reported in the paper were run on a Intel Core i7 computer running Ubuntu Desktop 18.04.5 and Java version 9. 
The description of the four workflows studied are available on-line through the Pegasus WorkflowGenerator: https://confluence.pegasus.isi.edu/display/pegasus/WorkflowHub. 
The implementation of the Q-learning algorithm was provided by the Brown-UMBC Reinforcement Learning and Planning (BURLAP) (http://burlap.cs.brown.edu/) Java code library version 3.0. 
Simulations were performed using the CloudSim (http://www.cloudbus.org/cloudsim/) simulator version 3.0. 
The statistical significance of the results was assessed using the implementation of the Mann-Whitney U test incorporated in Apache Commons Math (http://commons.apache.org/proper/commons-math/) version 3.3. 
Finally, Tableau Public (http://public.tableau.com) version 2018.3 and matplotlib (https://matplotlib.org/) version 3.2.2 were used to generate the visualizations.
```